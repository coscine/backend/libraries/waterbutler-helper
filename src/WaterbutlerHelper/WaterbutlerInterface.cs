﻿using Coscine.Configuration;
using Coscine.JwtHandler;
using Coscine.WaterbutlerHelper.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Coscine.WaterbutlerHelper
{
    public class WaterbutlerInterface
    {
        private readonly JWTHandler _jwtHandler;
        private readonly IConfiguration _configuration;
        private readonly IDataSourceService _dataSourceService;
        private readonly string _rdsKeyPrefix = "coscine/global/rds/ecs-rwth/rds";

        private readonly string _waterbuterUrl;
        private readonly string _rdsResourceHost;

        public WaterbutlerInterface()
        {
            _configuration = new ConsulConfiguration();
            _jwtHandler = new JWTHandler(_configuration);            
            _rdsResourceHost = _configuration.GetString($"{_rdsKeyPrefix}/s3_endpoint");
            _waterbuterUrl = _configuration.GetString("coscine/global/waterbutler_url");
            _dataSourceService = new DataSourceService(new HttpClient());
        }

        public WaterbutlerInterface(IConfiguration configuration)
        {
            _configuration = configuration;
            _jwtHandler = new JWTHandler(_configuration);

            _rdsResourceHost = _configuration.GetString($"{_rdsKeyPrefix}/s3_endpoint");
            _waterbuterUrl = _configuration.GetString("coscine/global/waterbutler_url");
            _dataSourceService = new DataSourceService(new HttpClient());
        }

        public WaterbutlerInterface(IConfiguration configuration, IDataSourceService dataSourceService)
        {
            _configuration = configuration;
            _jwtHandler = new JWTHandler(_configuration);

            _rdsResourceHost = _configuration.GetString($"{_rdsKeyPrefix}/s3_endpoint");
            _waterbuterUrl = _configuration.GetString("coscine/global/waterbutler_url");
            _dataSourceService = dataSourceService;
        }

        public WaterbutlerInterface(IConfiguration configuration, HttpClient httpClient)
        {
            _configuration = configuration;
            _jwtHandler = new JWTHandler(_configuration);

            _rdsResourceHost = _configuration.GetString($"{_rdsKeyPrefix}/s3_endpoint");
            _waterbuterUrl = _configuration.GetString("coscine/global/waterbutler_url");
            _dataSourceService = new DataSourceService(httpClient);
        }

        private string GetUrl(string provider, string path)
        {
            var splitPaths = path.Split('/');
            splitPaths = splitPaths.Select((splitPath) => Uri.EscapeDataString(splitPath)).ToArray();
            provider = IsLikeRdsS3(provider) || IsLikeRds(provider)? "rds" : provider;
            return $"{_waterbuterUrl}{provider}{string.Join("/", splitPaths)}";
        }

        // Parse MetaInfo
        public async Task<List<ObjectMetaInfo>> GetObjectInfoAsync(ObjectMetaInfo objectMetaInfo, string authHeader)
        {
            return await GetObjectInfoAsync(objectMetaInfo.Path, objectMetaInfo.Provider, authHeader);
        }

        public async Task<List<ObjectMetaInfo>> GetObjectInfoAsync(string path, string provider, string authHeader)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                path = "/";
            }

            var url = $"{GetUrl(provider, path)}?meta=";

            return await GetObjectInfoAsync(url, authHeader);
        }

        public async Task<List<ObjectMetaInfo>> GetObjectInfoAsync(string url, string authHeader)
        {
            var response = await _dataSourceService.GetWithHeader(url, authHeader);

            if (response.IsSuccessStatusCode)
            {
                var a = await response.Content.ReadAsStringAsync();
                var data = JObject.Parse(a)["data"];
                var infos = new List<ObjectMetaInfo>();

                if (data.Type == JTokenType.Array)
                {
                    foreach (var datum in data)
                    {
                        infos.Add(new ObjectMetaInfo(datum));
                    }
                }
                else
                {
                    infos.Add(new ObjectMetaInfo(data));
                }

                return infos;
            }
            else
            {
                return null;
            }
        }

        // Upload File
        public async Task<HttpResponseMessage> UploadFileAsync(string path, string filename, string provider, string authHeader, Stream data, long? contentLength = null)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                path = "/";
            }

            var url = $"{GetUrl(provider, path)}?kind=file&name={Uri.EscapeDataString(filename)}";

            return await _dataSourceService.PutWithHeader(url, authHeader, data, contentLength);
        }

        // Create Folder
        public async Task<HttpResponseMessage> CreateFolder(ObjectMetaInfo objectMetaInfo, string filename, string authHeader)
        {
            return await CreateFolder(objectMetaInfo.NewFolderLink, filename, authHeader);
        }

        public async Task<HttpResponseMessage> CreateFolder(string newFolderLink, string filename, string authHeader)
        {
            return await _dataSourceService.PutWithHeader($"{newFolderLink}&name={Uri.EscapeDataString(filename)}", authHeader);
        }

        // Delete Object
        public async Task<HttpResponseMessage> DeleteObjectAsync(ObjectMetaInfo objectMetaInfo, string authHeader)
        {
            return await DeleteObjectAsync(GetUrl(objectMetaInfo.Provider, objectMetaInfo.Path), authHeader);
        }

        public async Task<HttpResponseMessage> DeleteObjectAsync(string deleteLink, string authHeader)
        {
            return await _dataSourceService.DeleteWithHeader(deleteLink, authHeader);
        }

        // Download Object
        public async Task<HttpResponseMessage> DownloadObjectAsync(ObjectMetaInfo objectMetaInfo, string authHeader)
        {
            return await DownloadObjectAsync(GetUrl(objectMetaInfo.Provider, objectMetaInfo.Path), authHeader);
        }

        public async Task<HttpResponseMessage> DownloadObjectAsync(string downloadLink, string authHeader)
        {
            return await _dataSourceService.GetWithHeader(downloadLink, authHeader);
        }

        // Upload Object (also handles e.g. update of an object)
        public async Task<HttpResponseMessage> UploadObjectAsync(ObjectMetaInfo objectMetaInfo, string authHeader, Stream data, long? contentLength = null)
        {
            return await UploadObjectAsync($"{GetUrl(objectMetaInfo.Provider, objectMetaInfo.Path)}?kind=file", authHeader, data, contentLength);
        }

        public async Task<HttpResponseMessage> UploadObjectAsync(string uploadLink, string authHeader, Stream data, long? contentLength = null)
        {
            return await _dataSourceService.PutWithHeader(uploadLink, authHeader, data, contentLength);
        }

        public async Task<bool> Exists(string path, string provider, string authHeader)
        {
            if(string.IsNullOrWhiteSpace(path))
            {
                return false;
            }

            var url = $"{GetUrl(provider, path)}?meta=";

            var response = await _dataSourceService.GetWithHeader(url, authHeader);

            return response.IsSuccessStatusCode;

        }

        public bool IsFolder(string path, string provider, string authHeader)
        {
            var result = GetObjectInfoAsync(path, provider, authHeader).Result;
            if(result == null)
            {
                throw new Exception($"{path} does not exist.");
            }
            return result.Count > 1 || result.First().IsFolder;
        }

        public bool IsFile(string path, string provider, string authHeader)
        {
            var result = GetObjectInfoAsync(path, provider, authHeader).Result;
            if (result == null)
            {
                throw new Exception($"{path} does not exist.");
            }
            return result.Count == 1 && result.First().IsFile;
        }

        private string BuildWaterbutlerPayload(Dictionary<string, object> auth, Dictionary<string, object> credentials, Dictionary<string, object> settings)
        {
            var data = new Dictionary<string, object>
            {
                { "auth", auth },
                { "credentials", credentials },
                { "settings", settings },
                { "callback_url", "rwth-aachen.de" }
            };

            var payload = new JwtPayload
            {
                { "data", data }
            };

            return _jwtHandler.GenerateJwtToken(payload);
        }

        public string BuildAuthHeader(string resoureType, Dictionary<string, string> options)
        {
            string authHeader = null;
            if (IsLikeRds(resoureType))
            {
                authHeader = BuildRdsAuthHeader(options["bucketname"]);
            }
            else if (IsLikeRdsS3(resoureType))
            {
                authHeader = BuildRdsS3AuthHeader(options["accessKey"], options["secretKey"], options["bucketname"], options["endpoint"]);
            }
            else if (resoureType == "s3")
            {
                authHeader = BuildS3AuthHeader(options["accessKey"], options["secretKey"], options["bucketname"], options["resourceUrl"]);
            }
            else if (resoureType == "gitlab")
            {
                if (options.ContainsKey("host"))
                {
                    authHeader = BuildGitlabAuthHeader(options["token"], options["repositoryUrl"], options["repositoryNumber"], options["host"]);
                }
                else
                {
                    authHeader = BuildGitlabAuthHeader(options["token"], options["repositoryUrl"], options["repositoryNumber"]);
                }
            }

            return authHeader;
        }

        public string BuildRdsAuthHeader(string bucketname)
        {
            var auth = new Dictionary<string, object>();

            var credentials = new Dictionary<string, object>
            {
                { "access_key", _configuration.GetStringAndWait($"{_rdsKeyPrefix}/object_user_name") },
                { "secret_key", _configuration.GetStringAndWait($"{_rdsKeyPrefix}/object_user_secretkey") }
            };

            var settings = new Dictionary<string, object>
            {
                { "bucket", bucketname },
                { "host", _rdsResourceHost}
            };

            return BuildWaterbutlerPayload(auth, credentials, settings);
        }

        public string BuildRdsS3AuthHeader(string accessKey, string secretKey, string bucketname, string endpoint)
        {
            var auth = new Dictionary<string, object>();

            var credentials = new Dictionary<string, object>
            {
                { "access_key", accessKey },
                { "secret_key", secretKey }
            };

            var settings = new Dictionary<string, object>
            {
                { "bucket", bucketname },
                { "host", endpoint }
            };

            return BuildWaterbutlerPayload(auth, credentials, settings);
        }

        public string BuildS3AuthHeader(string accessKey, string secretKey, string bucketname, string resourceUrl)
        {
            var auth = new Dictionary<string, object>();

            var credentials = new Dictionary<string, object>
            {
                { "access_key", accessKey },
                { "secret_key", secretKey }
            };

            var settings = new Dictionary<string, object>
            {
                { "bucket", bucketname },
                { "host", resourceUrl }
            };

            return BuildWaterbutlerPayload(auth, credentials, settings);
        }

        public string BuildGitlabAuthHeader(string token, string repositoryUrl, string repositoryNumber, string host = "https://git.rwth-aachen.de")
        {

            var auth = new Dictionary<string, object>();

            var credentials = new Dictionary<string, object>
            {
                { "token", token }
            };

            var settings = new Dictionary<string, object>
            {
                { "owner", "Tester"},
                { "repo", repositoryUrl},
                { "repo_id", repositoryNumber.ToString()},
                { "host", host}
            };

            return BuildWaterbutlerPayload(auth, credentials, settings);
        }

        private bool IsLikeRds(string compare)
        {
            return compare == "rds" || compare == "rdsude" || compare == "rdsnrw" || compare == "rdstudo";
        }

        private bool IsLikeRdsS3(string compare)
        {
            return compare == "rdss3" || compare == "rdss3ude" || compare == "rdss3nrw" || compare == "rdss3tudo";
        }
    }
}
