﻿using Newtonsoft.Json.Linq;
using System;

namespace Coscine.WaterbutlerHelper
{
    public class ObjectMetaInfo
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public string Kind { get; set; }
        public string Provider { get; set; }
        public long? Size { get; set; }
        public DateTime? Modified { get; set; }
        public DateTime? Created { get; set; }
        public JToken Extra { get; set; }

        public bool IsFolder => string.Equals(Kind, "folder", StringComparison.OrdinalIgnoreCase);
        public bool IsFile => string.Equals(Kind, "file", StringComparison.OrdinalIgnoreCase);

        public string MoveLink { get; set; }
        public string UploadLink { get; set; }
        public string DeleteLink { get; set; }
        public string DownloadLink { get; set; }
        public string NewFolderLink { get; set; }

        public ObjectMetaInfo()
        {
        }

        public ObjectMetaInfo(JToken datum)
        {
            Name = datum["attributes"]["name"].ToString();
            Path = datum["attributes"]["path"].ToString();
            Kind = datum["attributes"]["kind"].ToString();
            Provider = datum["attributes"]["provider"].ToString();

            var tLong = datum["attributes"]["sizeInt"];
            Size = tLong == null || tLong.Type == JTokenType.Null ? null : (long?)long.Parse(tLong.ToString());

            var tModified = datum["attributes"]["modified_utc"];
            Modified = tModified == null || tModified.Type == JTokenType.Null ? null : (DateTime?)DateTime.Parse(tModified.ToString());

            var tCreated = datum["attributes"]["created_utc"];
            Created = tCreated == null || tCreated.Type == JTokenType.Null ? null : (DateTime?)DateTime.Parse(tCreated.ToString());

            Extra = datum["attributes"]["extra"];

            MoveLink = datum["links"]["move"].ToString();
            UploadLink = datum["links"]["upload"].ToString();
            DeleteLink = datum["links"]["delete"].ToString();
            // Optional
            DownloadLink = datum["links"]["download"]?.ToString();
        }
    }
}