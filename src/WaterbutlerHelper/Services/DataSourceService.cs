﻿using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Coscine.WaterbutlerHelper.Services
{
    public class DataSourceService : IDataSourceService
    {
        private readonly HttpClient _httpClient;

        public DataSourceService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        private async Task<HttpResponseMessage> RequestWithHeader(string url, string authHeader, HttpMethod method, Stream content = null, long? contentLength = null)
        {
            using var request = new HttpRequestMessage(method, url);
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", authHeader);

            if (content != null)
            {
                request.Content = new StreamContent(content);
                if (contentLength != null)
                {
                    request.Content.Headers.ContentLength = contentLength;
                }
            }

            // Thread safe according to MSDN and HttpCompletionOption sets it to get only headers first.
            return await _httpClient.SendAsync(request, HttpCompletionOption.ResponseHeadersRead).ConfigureAwait(false);
        }

        public async Task<HttpResponseMessage> GetWithHeader(string url, string authHeader)
        {
            return await RequestWithHeader(url, authHeader, HttpMethod.Get).ConfigureAwait(false);
        }

        public async Task<HttpResponseMessage> DeleteWithHeader(string url, string authHeader)
        {
            return await RequestWithHeader(url, authHeader, HttpMethod.Delete).ConfigureAwait(false);
        }

        public async Task<HttpResponseMessage> PutWithHeader(string url, string authHeader, Stream stream = null, long? contentLength = null)
        {
            return await RequestWithHeader(url, authHeader, HttpMethod.Put, stream, contentLength).ConfigureAwait(false);
        }
    }
}