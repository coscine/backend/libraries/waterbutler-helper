﻿using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace Coscine.WaterbutlerHelper.Services
{
    public interface IDataSourceService
    {
        Task<HttpResponseMessage> GetWithHeader(string url, string authHeader);
        Task<HttpResponseMessage> DeleteWithHeader(string url, string authHeader);
        Task<HttpResponseMessage> PutWithHeader(string url, string authHeader, Stream stream = null, long? contentLength = null);
    }
}
