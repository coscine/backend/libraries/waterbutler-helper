﻿using System;
using System.Web;

namespace Coscine.WaterbutlerHelper.ReturnObjects
{
    public class ObjectMetaInfoReturnObject
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public long? Size { get; set; }
        public string Kind { get; set; }
        public DateTime? Modified { get; set; }
        public DateTime? Created { get; set; }
        public string Provider { get; set; }

        public bool IsFolder => string.Equals(Kind, "folder", StringComparison.OrdinalIgnoreCase);
        public bool IsFile => string.Equals(Kind, "file", StringComparison.OrdinalIgnoreCase);

        public string UploadLink { get; set; }
        public string DeleteLink { get; set; }
        public string DownloadLink { get; set; }

        public ObjectMetaInfoReturnObject()
        {
        }

        public ObjectMetaInfoReturnObject(ObjectMetaInfo objectMetaInfo, string blobApiLink, string resource)
        {
            Name = objectMetaInfo.Name;
            Path = objectMetaInfo.Path;
            Size = objectMetaInfo.Size;
            Kind = objectMetaInfo.Kind;
            Modified = objectMetaInfo.Modified;
            Created = objectMetaInfo.Created;
            Provider = objectMetaInfo.Provider;

            var encodedUrl = HttpUtility.UrlEncode(Path[1..]);
            UploadLink = $"{blobApiLink}{resource}/{encodedUrl}";
            DeleteLink = $"{blobApiLink}{resource}/{encodedUrl}";
            DownloadLink = $"{blobApiLink}{resource}/{encodedUrl}";
        }
    }
}