﻿using Coscine.Configuration;
using Coscine.ECSManager;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Coscine.WaterbutlerHelper.Tests
{
    [TestFixture]
    public class WaterbutlerTests
    {
        private readonly string _testPrefix = "Coscine-WaterbutlerHelper-Tests";
        private readonly IConfiguration _configuration = new ConsulConfiguration();

        private static readonly Dictionary<string, string> _configs = new Dictionary<string, string>
        {
            // RDS-Web
            { "rds", "coscine/global/rds/ecs-rwth/rds" },
            { "rdsude", "coscine/global/rds/ecs-ude/rds" },
            { "rdsnrw", "coscine/global/rds/ecs-nrw/rds" },
            { "rdstudo", "coscine/global/rds/ecs-tudo/rds" },
            // RDS-S3
            { "rdss3", "coscine/global/rds/ecs-rwth/rds-s3" },
            { "rdss3ude", "coscine/global/rds/ecs-ude/rds-s3" },
            { "rdss3nrw", "coscine/global/rds/ecs-nrw/rds-s3" },
            { "rdss3tudo", "coscine/global/rds/ecs-tudo/rds-s3" }
        };

        private static IEnumerable<object[]> GetTestConfigs()
        {
            foreach (var kv in _configs)
            {
                yield return new object[] { kv.Key, kv.Value };
            }
        }

        private long _quota;

        private Guid _guid;
        private string _bucketName;

        [SetUp]
        public void SetUp()
        {
            _quota = 10;
            _guid = Guid.NewGuid();
            _bucketName = $"{_testPrefix}-{_guid}";
        }

        private EcsManager CreateManager(string prefix)
        {
            return new EcsManager
            {
                EcsManagerConfiguration = new EcsManagerConfiguration
                {
                    ManagerApiEndpoint = _configuration.GetString($"{prefix}/manager_api_endpoint"),
                    NamespaceName = _configuration.GetString($"{prefix}/namespace_name"),
                    NamespaceAdminName = _configuration.GetString($"{prefix}/namespace_admin_name"),
                    NamespaceAdminPassword = _configuration.GetString($"{prefix}/namespace_admin_password"),
                    ObjectUserName = _configuration.GetString($"{prefix}/object_user_name"),
                    ReplicationGroupId = _configuration.GetString($"{prefix}/replication_group_id"),
                }
            };
        }

        private Dictionary<string, string> GetResourceOptions(string prefix)
        {
            var dictionary = new Dictionary<string, string>
            {
                { "accessKey", _configuration.GetString($"{prefix}/object_user_name") },
                { "secretKey", _configuration.GetString($"{prefix}/object_user_secretkey") },
                { "bucketname", _bucketName },
                { "endpoint", _configuration.GetString($"{prefix}/s3_endpoint") }
            };
            return dictionary;
        }

        [TearDown]
        public void TearDown()
        {
            foreach (var kv in _configs)
            {
                var ecsManager = CreateManager(kv.Value);
                try
                {
                    ecsManager.DeleteBucket(_bucketName).Wait();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        [TestCaseSource(nameof(GetTestConfigs))]
        public void UploadDownloadDeleteTest(string provider, string configPrefix)
        {
            var manager = CreateManager(configPrefix);

            manager.CreateBucket(_bucketName, _quota).Wait();

            var options = GetResourceOptions(configPrefix);

            var waterbuterInterface = new WaterbutlerInterface();
            var authHeader = waterbuterInterface.BuildAuthHeader(provider, options);

            var randomFileName = Guid.NewGuid();

            var memoryStream = new MemoryStream();
            byte[] testData = { (byte)'C', (byte)'o', (byte)'S', (byte)'I', (byte)'n', (byte)'e' };

            memoryStream.Write(testData, 0, testData.Length);
            memoryStream.Position = 0;

            var uploadFileResponse = waterbuterInterface.UploadFileAsync("/", randomFileName.ToString(), provider, authHeader, memoryStream).Result;

            Assert.True(uploadFileResponse.IsSuccessStatusCode);
            Assert.True(waterbuterInterface.Exists($"/{randomFileName}", provider, authHeader).Result);

            var objectMetadata = waterbuterInterface.GetObjectInfoAsync("/", provider, authHeader).Result;
            var objectInfo = objectMetadata.First(x => x.Name == randomFileName.ToString());

            using (var dwonloadObjectResponse = waterbuterInterface.DownloadObjectAsync(objectInfo, authHeader).Result)
            {
                Assert.True(dwonloadObjectResponse.IsSuccessStatusCode);
                var temp = new byte[testData.Length];
                dwonloadObjectResponse.Content.ReadAsStreamAsync().Result.Read(temp, 0, testData.Length);
                Assert.True(testData.SequenceEqual(temp));
            }

            var deleteObjectResponse = waterbuterInterface.DeleteObjectAsync(objectInfo, authHeader).Result;

            Assert.True(deleteObjectResponse.IsSuccessStatusCode);
            Assert.False(waterbuterInterface.Exists($"/{randomFileName}", provider, authHeader).Result);
        }

        [TestCase(1, 10)] // 1 Kb
        [TestCase(1, 20)] // 1 Mb
        [TestCase(10, 20)] // 10 Mb
        [TestCase(1, 30)] // 1 GB
        //[TestCase(2, 30)] // 2 GB
        //[TestCase(4, 30)] // 4 GB
        //[TestCase(5, 30)] // 5 GB
        public void LargeUpload(int multiplyer, int power, string provider = "rds", string configPrefix = "coscine/global/rds/ecs-rwth/rds")
        {
            var manager = CreateManager(configPrefix);

            manager.CreateBucket(_bucketName, _quota).Wait();
            var waterbuterInterface = new WaterbutlerInterface();
            var authHeader = waterbuterInterface.BuildRdsAuthHeader(_bucketName);
            var randomFileName = Guid.NewGuid();

            var seed = Math.Abs((int)DateTime.Now.Ticks);
            var semiRandomStream = new SemiRandomStream(multiplyer * (int)Math.Pow(2, power), seed, 1024 * 1024 * 50);

            var uploadFileResponse = waterbuterInterface.UploadFileAsync("/", randomFileName.ToString(), provider, authHeader, semiRandomStream).Result;

            Assert.True(uploadFileResponse.IsSuccessStatusCode);
            Assert.True(waterbuterInterface.Exists($"/{randomFileName}", provider, authHeader).Result);

            var objectMetadata = waterbuterInterface.GetObjectInfoAsync("/", provider, authHeader).Result;
            var objectInfo = objectMetadata.First(x => x.Name == randomFileName.ToString());

            Assert.True(objectInfo.Size == semiRandomStream.Length);

            var deleteObjectResponse = waterbuterInterface.DeleteObjectAsync(objectInfo, authHeader).Result;

            Assert.True(deleteObjectResponse.IsSuccessStatusCode);
            Assert.False(waterbuterInterface.Exists($"/{randomFileName}", provider, authHeader).Result);
        }
    }
}