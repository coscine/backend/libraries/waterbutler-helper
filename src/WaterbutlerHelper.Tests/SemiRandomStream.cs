﻿using System;
using System.IO;

namespace Coscine.WaterbutlerHelper.Tests
{
    public class SemiRandomStream : Stream
    {
        private readonly long _length;
        public int BufferSize
        {
            set
            {
                _buffer = new byte[value];
                _random.NextBytes(_buffer);
            }
            get { return _buffer.Length; }
        }

        private readonly Random _random;
        private byte[] _buffer;

        public override bool CanRead => true;

        public override bool CanSeek => true;

        public override bool CanWrite => false;

        public override long Length => _length;

        public override long Position { get; set; }

        public SemiRandomStream(long length, int seed, int bufferSize)
        {
            _length = length;
            Position = 0;
            _random = new Random(seed);
            BufferSize = bufferSize;
        }

        public override void Flush()
        {
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            var n = _length - Position;
            if (n > count) n = count;
            if (n <= 0)
                return 0;

            if (n <= 8)
            {
                var byteCount = n;
                while (--byteCount >= 0)
                {
                    buffer[offset + byteCount] = _buffer[Position + byteCount];
                }
            }
            else
            {
                Array.Copy(_buffer, buffer, count);
            }

            Position += n;
            return count;
        }

        public override long Seek(long offset, SeekOrigin loc)
        {
            switch (loc)
            {
                case SeekOrigin.Begin:
                    {
                        long tempPosition = offset;
                        Position = tempPosition;
                        break;
                    }
                case SeekOrigin.Current:
                    {
                        long tempPosition = Position + offset;
                        Position = tempPosition;
                        break;
                    }
                case SeekOrigin.End:
                    {
                        long tempPosition = _length + offset;
                        Position = tempPosition;
                        break;
                    }
                default:
                    throw new ArgumentException("");
            }

            return Position;
        }

        public override void SetLength(long value)
        {
#pragma warning disable RCS1079 // Throwing of new NotImplementedException.
            throw new NotImplementedException();
#pragma warning restore RCS1079 // Throwing of new NotImplementedException.
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
#pragma warning disable RCS1079 // Throwing of new NotImplementedException.
            throw new NotImplementedException();
#pragma warning restore RCS1079 // Throwing of new NotImplementedException.
        }

        public override void Close()
        {
        }
    }
}

